package passwordValidator;
/*
 * Gagandeep Ghotra
 * 991528423
 * This class is going to store the methods that will help in 
 * validating passwords. Created using TDD.
 */
public class PasswordValidator {
	
	public static boolean isValidPassword(String password) {
		int numberOfDigits = 0;
		
		for(int i = 0; i < password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) numberOfDigits++;
		}
		
		if (password.contains(" "))
			throw new NumberFormatException("Invalid Password!");
		
		else if (password.length() < 8)
			throw new NumberFormatException("Password should be atleast be 8 characters!");
		
		
		else if((numberOfDigits == 0) || (numberOfDigits == 3))
			throw new NumberFormatException("Passwords needs to have atleast 2 digits!");
		
		return true;
	}
	
	public static boolean isValidCharacterCase(String password) {
		return password != null 
				&& password.matches("^.*[a-z].*$")
				&& password.matches("^.*[A-Z].*$");
	}
	
}
