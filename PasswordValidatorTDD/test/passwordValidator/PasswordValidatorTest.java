package passwordValidator;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * Gagandeep Ghotra
 * 991528423
 * This class is going to store the methods that will
 * test and aid in TDD of the actual password
 * validator methodS. Created using TDD.
 */
public class PasswordValidatorTest {
	
	@Test
	public void testIsValidCharacterCaseRegular() {
		boolean isValidCase = PasswordValidator.isValidCharacterCase("abcABC");
		assertTrue("Invalid Character Case!", isValidCase == true);
	}
	
	@Test
	public void testIsValidCharacterCaseException() {
		boolean isValidCase = PasswordValidator.isValidCharacterCase("abc123");
		assertFalse("Invalid Character Case!", isValidCase == true);
	}
	
	@Test
	public void testIsValidCharacterCaseBoundaryIn() {
		boolean isValidCase = PasswordValidator.isValidCharacterCase("aB");
		assertTrue("Invalid Character Case!", isValidCase == true);
	}
	
	@Test
	public void testIsValidCharacterUpperCaseBoundaryOut() {
		boolean isValidCase = PasswordValidator.isValidCharacterCase("ABCDEF");
		assertFalse("Invalid Character Case!", isValidCase == true);
	}
	
	@Test
	public void testIsValidCharacterLowerCaseBoundaryOut() {
		boolean isValidCase = PasswordValidator.isValidCharacterCase("abcdef");
		assertFalse("Invalid Character Case!", isValidCase == true);
	}
	
	
	/*
	@Test(expected=NumberFormatException.class)
	public void testIsValidPasswordBoundaryOut() {
		boolean isValidPassword = PasswordValidator.isValidPassword("abcdefgh1");
		fail("Invalid Password!");
	}	
	
	@Test
	public void testIsValidPasswordBoundaryIn() {
		boolean isValidPassword = PasswordValidator.isValidPassword("abcdef12");
		assertTrue("Invalid Password!", isValidPassword == true);
	}
		
	@Test
	public void testIsValidPasswordRegular() {
		boolean isValidPassword = PasswordValidator.isValidPassword("abcdefghi12");
		assertTrue("Invalid Password!", isValidPassword == true);
	}
	
    @Test(expected=NumberFormatException.class)
	public void testIsValidPasswordException() {
		boolean isValidPassword = PasswordValidator.isValidPassword("abc123");
		fail("Invalid Password!");
	}*/

}

/*


	@Test(expected=NumberFormatException.class)
	public void testIsValidPasswordBoundaryOut() {
		boolean isValidPassword = PasswordValidator.isValidPassword("abcdefg");
		fail("Invalid Password!");
	}	
	

	
    @Test(expected=NumberFormatException.class)
	public void testIsValidPasswordException() {
		boolean isValidPassword = PasswordValidator.isValidPassword("abcdefghijk ");
		fail("Invalid Password!");
	}
	






*/